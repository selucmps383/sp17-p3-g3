import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ApiService } from './api.service';
import { MatchService } from './matches/match.service';

import { AppRoutingModule } from './app-routing.module';
import { SummonerComponent } from './summoner/summoner.component';
import { MatchListComponent } from './matches/match-list.component';
import { MatchDetailComponent } from './matches/match-detail.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    JsonpModule
  ],
  declarations: [
    AppComponent,
    SummonerComponent,
    MatchListComponent,
    MatchDetailComponent
  ],
  providers: [ ApiService, MatchService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
