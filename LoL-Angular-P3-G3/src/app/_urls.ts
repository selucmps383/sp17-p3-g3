export const apiCall = {
    baseUrl: '',
    summoner: '',
    matchList: '',
    match: ''
};

export const staticImage = {
    baseUrl: 'http://ddragon.leagueoflegends.com/cdn/7.5.2/img/',
    profileIcon: this.baseUrl + 'profileicon/',
    championsSquare: this.baseUrl + 'champion/',
    passiveAbilities: this.baseUrl + 'passive/',
    championAbilities: this.baseUrl + 'spell/',
    summonerSpells: this.baseUrl + 'spell/', // Don't think this is right, but its what the reference had
    items: this.baseUrl + 'item/',
    masteries: this.baseUrl + 'mastery/',
    runes: this.baseUrl + 'rune/',
    sprites: this.baseUrl + 'sprite/'
};

export const staticData = {};
