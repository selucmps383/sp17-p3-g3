import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { Summoner } from './summoner/summoner';
import { config } from './_config';
import { Match } from './matches/match';
import { MatchDetail } from './matches/match-detail';

@Injectable()
export class ApiService {
  private apikey = config.apiKey;
  private summonerUrl = 'https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/';  // URL to web API
  private matchListUrl = 'https://na.api.pvp.net/api/lol/na/v2.2/matchlist/by-summoner/';
  private matchDetailUrl = 'https://na.api.pvp.net/api/lol/na/v2.2/match/';
  private characterUrl = 'https://global.api.pvp.net/api/lol/static-data/na/v1.2/champion/';

  constructor (private http: Http) {}

  getSummoners (name: string): Observable<Summoner[]> {
    return this.http.get(this.summonerUrl + name + '?api_key=' + this.apikey)
                    .map((response: Response) => {
                        return <Summoner>response.json();
                    })
                    .catch(this.handleError);
  }

  getSummoner (name: string): Observable<Summoner> {
    return this.http.get(this.summonerUrl + name + '?api_key=' + this.apikey)
                    .map((response: Response) => {
                        return <Summoner>response.json();
                    })
                    .catch(this.handleError);
  }

  getMatchList (id: number): Observable<Match[]> {
    return this.http.get(this.matchListUrl + id + '?beginIndex=0&endIndex=4&api_key=' + this.apikey)
                    .map((response: Response) => {
                        return <Match[]>response.json();
                    })
                    .catch(this.handleError);
  }

  getMatchDetails (id: number): Observable<MatchDetail[]> {
    console.log('service ' + this.matchDetailUrl + id + '?api_key=' + this.apikey);
    return this.http.get(this.matchDetailUrl + id + '?api_key=' + this.apikey)
      .map((respone: Response) => {
        return <MatchDetail[]>respone.json();
      })
      .catch(this.handleError);

  }

  getCharacter (id: number): Observable<any> {
    console.log('service ' + this.characterUrl + id + '?champData=image&api_key=' + this.apikey);
    return this.http.get(this.characterUrl + id + '?champData=image&api_key=' + this.apikey)
      .map((respone: Response) => {
        return <any[]>respone.json();
      })
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || { };
  }
  
  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
