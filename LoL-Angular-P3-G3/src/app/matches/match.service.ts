import { Injectable } from '@angular/core';
import { Http, Response }          from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { Match } from './match';
import { config } from '../_config';

@Injectable()
export class MatchService {
  private apikey = config.apiKey;
  private matchListUrl = 'https://na.api.pvp.net/api/lol/na/v2.2/matchlist/by-summoner/';

  constructor(private http: Http) { }

  getMatchList (id: number): Observable<Match[]> {
    return this.http.get(this.matchListUrl + id + '?beginIndex=0&endIndex=4&api_key=' + this.apikey)
                    .map((response: Response) => {
                        return <Match[]>response.json();
                    })
                    .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || { };
  }
  
  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}

