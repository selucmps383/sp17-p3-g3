export class MatchDetail {
    matchId: number;
    region: string;
    platformId: number;
    matchMode: string;
    matchType: string;
    matchCreation: number;
    matchDuration: number;
    queueType: string;
    mapId: number;
    season: string;
    matchVersion: number;
    participants: string[];
    participantIdentities: string[];
    teams: string[];

}