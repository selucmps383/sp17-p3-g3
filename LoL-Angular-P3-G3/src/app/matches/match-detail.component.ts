import { Component, Input } from '@angular/core';

import { MatchDetail } from './match-detail';

import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-match-detail',
  templateUrl: './templates/match-detail.component.html',
  styleUrls: ['./css/match-detail.component.css'],
})
export class MatchDetailComponent {
  @Input() matchDeet: any[];

}
