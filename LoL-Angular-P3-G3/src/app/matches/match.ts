export class Match {
    region: string;
    platformId: string;
    matchId: number;
    champion: number;
    queue: string;
    season: string;
    timestamp: number;
    lane: string;
    role: string;
}


