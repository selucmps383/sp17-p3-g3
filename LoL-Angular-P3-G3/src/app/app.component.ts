import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Summoner } from './summoner/summoner';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './templates/app.component.html',
  styleUrls: ['./css/app.component.css'],
  providers: [ ApiService ]
})
export class AppComponent {

    errorMessage: string;
    title = 'League of Legends';
    welcomeMessage: string = "League of Legends!";
    form: FormGroup;
    summonerName = "";
    summoner = new Summoner;

    

    constructor(
        private apiService: ApiService
    ){}

    getSummoner() {
         console.log(this.summoner.name)

          this.apiService.getSummoner(this.summoner.name)
                     .subscribe(
                       summoner => this.summoner = summoner[this.summoner.name],                       
                       error =>  this.errorMessage = <any>error);
     }
}
