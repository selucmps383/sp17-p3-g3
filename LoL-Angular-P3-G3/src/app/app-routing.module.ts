import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatchDetailComponent } from './matches/match-detail.component';

import { SummonerComponent }  from './summoner/summoner.component';

const routes: Routes = [
  { path: 'summoner/:name', component: SummonerComponent },
  { path: 'match-detail/:id', component: MatchDetailComponent }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
