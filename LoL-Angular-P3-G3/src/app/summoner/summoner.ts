import { Match } from '../matches/match';
import { ApiService } from '../api.service';

export class Summoner {
    apiService = ApiService;
    id: number;
    name: string;
    profileIconId: number;
    revisionDate: number;
    summonerLevel: number;
    matches: Match[];
}
