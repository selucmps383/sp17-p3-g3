import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { Summoner } from './summoner';
import { ApiService } from '../api.service';
import { Match } from '../matches/match';
import { MatchDetail } from '../matches/match-detail';

@Component({
  selector: 'summoner',
  templateUrl: './templates/summoner.component.html',
  styleUrls: ['./css/summoner.component.css'],
  providers: [ApiService]
})
export class SummonerComponent implements OnInit, OnDestroy {
  errorMessage: string;
  nameParam: string;
  private sub: any;
  summoner: Summoner[];
  matches: Match[];
  matchDetail: MatchDetail[];

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private location: Location
    ) 
  {}  

  ngOnInit() { 
    this.sub = this.route.params.subscribe((params: Params) => this.nameParam = params['name']);

    this.getSummoner();    

  }

  goBack(): void {
    this.location.back();
  }
  getSummoner() {
    this.apiService.getSummoner(this.nameParam)
                     .subscribe(
                       summoner => this.summoner = summoner[this.nameParam],                       
                       error =>  this.errorMessage = <any>error);
  }

  getMatch(id) {
    this.apiService.getMatchDetails(id)
    .subscribe(
      matchDetail => this.matchDetail = matchDetail,
      error => this.errorMessage = <any>error
    );
  }

  getMatches() {
    this.route.params
      .switchMap((params: Params) => this.apiService.getMatchList(this.summoner['id']))
      .subscribe(matches => this.matches = matches['matches'])
    console.log(this.summoner['id']);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
