import { LoLAngularP3G3Page } from './app.po';

describe('lo-l-angular-p3-g3 App', function() {
  let page: LoLAngularP3G3Page;

  beforeEach(() => {
    page = new LoLAngularP3G3Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
