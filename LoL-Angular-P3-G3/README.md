# _Very important API Key information_

_This is going to be a breaking change if you do not follow this_

After pulling or updating your repo from the master branch, do the following:

* Go to the `/app` directory.
* Make a copy of `_config.ts.example` and name it `_config.ts`.
* Please take note that the original file should not be deleted or changed. The only difference is a new file named `_config.ts` which is not tracked by version control, but is referenced in the project.
* In the new `_config.ts` file, notice this block of code :

`
export const config = {
  apiKey: 'your-key-here'
};
`
* Replace the value of `apiKey` with your actual [api key from your Riot account](https://developer.riotgames.com/docs/api-keys). 
* If you don't follow this, the project will either not run, or you will override the project files in an undesirable way...or both.



# LoLAngularP3G3

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.28.3.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to GitHub Pages

Run `ng github-pages:deploy` to deploy to GitHub Pages.

## Further help

To get more help on the `angular-cli` use `ng help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
